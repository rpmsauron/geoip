<?php

namespace Tests\ApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class ApiControllerTest extends WebTestCase
{
    const CONTENT_TYPE_VALUE  = "application/json";
    const CONTENT_TYPE_HEADER = "CONTENT_TYPE";

    /**
     * @var Client $client
     */
    private $client;

    /**
     * Tests getting Country.
     */
    public function testGetCountryAction()
    {
        $route   = "/api/locationByIP?IP=2.16.7.1";
        $headers = [self::CONTENT_TYPE_HEADER => self::CONTENT_TYPE_VALUE];

        $this->client = static::createClient();
        $this->client->request("GET", $route, [], [], $headers);
        $response = $this->client->getResponse();

        $expectedResult = '{'
            . '"ipmin":"2.16.6.0",'
            . '"ipmax":"2.16.7.255",'
            . '"ipdecmin":"34604544",'
            . '"ipdecmax":"34605055",'
            . '"countrycode":"DE",'
            . '"country":"Germany"'
            . '}';

        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
        $this->assertSame(self::CONTENT_TYPE_VALUE, $response->headers->get("Content-Type"));
        $this->assertEquals($expectedResult, $response->getContent());
        $this->assertNotEmpty($response->getContent());
    }
}
