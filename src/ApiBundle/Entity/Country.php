<?php

namespace ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ApiBundle\Repository\CountryRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="country")
 */
class Country implements \JsonSerializable
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="ipmin", type="string", length=15, nullable=false)
     */
    private $ipmin;

    /**
     * @ORM\Column(name="ipmax", type="string", length=15, nullable=false)
     */
    private $ipmax;

    /**
     * @ORM\Column(name="ipdecmin", type="string", length=10, nullable=false)
     */
    private $ipdecmin;

    /**
     * @ORM\Column(name="ipdecmax", type="string", length=10, nullable=false)
     */
    private $ipdecmax;
    
    /**
     * @ORM\Column(name="countrycode", type="string", length=3, nullable=false)
     */
    private $countrycode;

    /**
     * @ORM\Column(name="country", type="string", length=150, nullable=false)
     */
    private $country;

    /**
     * Constructs a new Country instance
     *
     * @param string $ipmin       The min IP value
     * @param string $ipmax       The max IP value
     * @param string $ipdecmin    The min decimal IP value
     * @param string $ipdecmax    The max decimal IP value
     * @param string $countrycode The country code
     * @param string $country     The country
     */
    public function __construct(
        string $ipmin,
        string $ipmax,
        string $ipdecmin,
        string $ipdecmax,
        string $countrycode,
        string $country
    ) {
        $this->ipmin       = $ipmin;
        $this->ipmax       = $ipmax;
        $this->ipdecmin    = $ipdecmin;
        $this->ipdecmax    = $ipdecmax;
        $this->countrycode = $countrycode;
        $this->country     = $country;
        
    }

    /**
     * Returns the id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Returns the min IP value
     *
     * @return string
     */
    public function getIpmin(): string
    {
        return $this->ipmin;
    }

    /**
     * Returns the max IP value
     *
     * @return string
     */
    public function getIpmax(): string
    {
        return $this->ipmax;
    }

    /**
     * Returns the min decimal IP value
     *
     * @return string
     */
    public function getIpdecmin(): string
    {
        return $this->ipdecmin;
    }
    
    /**
     * Returns the max decimal IP value
     *
     * @return string
     */
    public function getIpdecmax(): string
    {
        return $this->ipdecmax;
    }

    /**
     * Returns the country code
     *
     * @return string
     */
    public function getCountrycode(): string
    {
        return $this->countrycode;
    }

    /**
     * Returns the country
     *
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * Updates the min IP value
     *
     * @param  string  $ipmin The new value for the min IP
     *
     * @return Country This Country
     */
    public function setIpmin(string $ipmin): Country
    {
        $this->ipmin = $ipmin;

        return $this;
    }

    /**
     * Updates the max IP value
     *
     * @param  string  $ipmax The new value for the max IP
     *
     * @return Country This Country
     */
    public function setIpmax(string $ipmax): Country
    {
        $this->ipmax = $ipmax;

        return $this;
    }

    /**
     * Updates the min decimal IP value
     *
     * @param  string  $ipdecmin The new value for the min decimal IP
     *
     * @return Country This Country
     */
    public function setIpdecmin(string $ipdecmin): Country
    {
        $this->ipdecmin = $ipdecmin;

        return $this;
    }

    /**
     * Updates the max decimal IP value
     *
     * @param  string  $ipdecmax The new value for the max decimal IP
     *
     * @return Country This Country
     */
    public function setIpdecmax(string $ipdecmax): Country
    {
        $this->ipdecmax = $ipdecmax;

        return $this;
    }

    /**
     * Updates the country code
     *
     * @param  string  $countrycode The new value for the countrycode
     *
     * @return Country This Country
     */
    public function setCountrycode(string $countrycode): Country
    {
        $this->countrycode = $countrycode;

        return $this;
    }

    /**
     * Updates the country
     *
     * @param  string  $country The new value for the country
     *
     * @return Country This Country
     */
    public function setCountry(string $country): Country
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Defines the JSON structure of instance of this class when calling json_encode on it
     *
     * @return array
     */
    public function jsonSerialize(): array
    {
        return [
            'ipmin'       => $this->ipmin,
            'ipmax'       => $this->ipmax,
            'ipdecmin'    => $this->ipdecmin,
            'ipdecmax'    => $this->ipdecmax,
            'countrycode' => $this->countrycode,
            'country'     => $this->country,
        ];
    }
}
