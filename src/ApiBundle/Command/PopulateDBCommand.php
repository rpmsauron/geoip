<?php

namespace ApiBundle\Command;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use ZipArchive;
use ApiBundle\Entity\Country;

/**
 * Command to populate the DB with data
 */
class PopulateDBCommand extends ContainerAwareCommand
{
    private $entityManager;

    /**
     * Configuration of the Command
     */
    protected function configure()
    {
        $this->setName('db:populate')
            ->setDescription('Populates DB data from downloaded CSV file.');
    }

    /**
     * Executes the Command
     *
     * @param InputInterface  $input  Input interface to read Command arguments from CLI
     * @param OutputInterface $output Output interface to write data to CLI
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container           = $this->getContainer();
        $doctrine            = $container->get('doctrine');
        $this->entityManager = $doctrine->getEntityManager();

        $url = "http://geolite.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip";
        $storagePath = "var/storage/GeoIPCountryCSV.zip";
        $extractionPath = "var/storage/";
        $extractionFilename = "GeoIPCountryWhois.csv"; //auto-defined from within archive

        $countryRepository = $this->entityManager->getRepository("ApiBundle:Country");
        $countries = $countryRepository->findAll();

        if (empty($countries)) {
            $output->writeln("Database empty. Retrieving data source file from remote server.");
            $this->getFile($url, $storagePath);
            $output->writeln("Uncompressing file.");           
            $archive = new ZipArchive;
            $archiveExists = $archive->open($storagePath);

            if ($archiveExists) {
                $archive->extractTo($extractionPath);
                $archive->close();
                $output->writeln("File extracted successfully.");
            } else {
                $output->writeln("Problem occurred on extracting file.");

                return;
            }

            $output->writeln("Populating database.");
            $this->loadFromSpreadsheet($extractionPath . $extractionFilename);
        } else {
            $output->writeln("Database was not touched. It already contains data.");
        }
        $output->writeln("Execution finished");
    }

    /**
     * Downloads data source file
     *
     * @param string $url         The remote file URL
     * @param string $destination The path of the file
     *
     * @return bool Returns true if file exists for download, or false otherwise
     */
    private function getFile(string $url, string $destination)
    {
        $fp = fopen ($destination, 'w+');
        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_BINARYTRANSFER, true );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, false );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch, CURLOPT_CONNECTTIMEOUT, 10 );
        curl_setopt( $ch, CURLOPT_FILE, $fp );
        curl_exec( $ch );
        curl_close( $ch );
        fclose( $fp );

        if (filesize($destination) > 0) {
            return true;
        }

        return false;
    }

    /**
     * Loads data from spreadsheet file onto the database
     *
     * @param string $spreadsheetPath The path of the spreadsheet file
     *
     * @return bool Returns true if successful import occurred
     */
    private function loadFromSpreadsheet(string $spreadsheetPath)
    {
        $count  = 0;
        $offset = 500;

        $objPHPExcel = \PHPExcel_IOFactory::load($spreadsheetPath);

        foreach ($objPHPExcel->getWorksheetIterator() as $worksheet) {
            foreach ($worksheet->getRowIterator() as $row) {
                foreach ($row->getCellIterator() as $cell) {
                    if (!is_null($cell)) {
                        $column = substr($cell->getCoordinate(), 0, 1);
                        switch ($column) {
                            case "A":
                                $ipmin = $cell->getCalculatedValue();
                                break;
                            case "B":
                                $ipmax = $cell->getCalculatedValue();
                                break;
                            case "C":
                                $ipdecmin = $cell->getCalculatedValue();
                                break;
                            case "D":
                                $ipdecmax = $cell->getCalculatedValue();
                                break;
                            case "E":
                                $countrycode = $cell->getCalculatedValue();
                                break;
                            case "F":
                                $country = $cell->getCalculatedValue();
                                break;
                        }
                    }
                }

                $country = new Country($ipmin, $ipmax, $ipdecmin, $ipdecmax, $countrycode, $country);
                $this->entityManager->persist($country);
                if ($count % $offset == 0) {
                    $this->entityManager->flush();
                    $this->entityManager->clear();
                }
                $count++;
            }
        }
        $this->entityManager->flush();
        $this->entityManager->clear();

        return true;
    }
}
