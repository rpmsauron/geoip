<?php

namespace ApiBundle\Controller;

use FOS\RestBundle\Controller\Annotations as HTTPAnnotations;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use ApiBundle\Repository\CountryRepository;
use Symfony\Component\HttpKernel\Exception;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Operation;

class ApiController extends FOSRestController
{
    /**
     * @var CountryRepository
     */
    private $countryRepository;

    /**
     * Creates a new ApiController.
     *
     * @param CountryRepository $countryRepository The Country Repository.
     */
    public function __construct(CountryRepository $countryRepository)
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * Gets a Country from an IP address
     *
     * ### Request Example ###
     *
     * @HTTPAnnotations\Get(path="/locationByIP")
     * @Operation(
     *     summary="Returns a Country based on an IP address",
     *     @SWG\Response(
     *         response="404",
     *         description="Returned when no URL parameter passed or when no Country found for given IP address"
     *     ),
     *     @SWG\Response(
     *         response="200",
     *         description="Returned when successful",
     *         @SWG\Property(
     *             type="json",
     *             example={
     *                 "ipmin": "128.0.0.0",
     *                 "ipmax": "128.0.0.255",
     *                 "ipdecmin": "2147483648",
     *                 "ipdecmax": "2147483903",
     *                 "countrycode": "NL",
     *                 "country": "Netherlands"
     *             }
     *         )
     *     ),
     *     @SWG\Parameter(
     *         name="IP",
     *         description="IP address to fetch Country for",
     *         in="query",
     *         type="string",
     *         required=true
     *     )
     * )
     *
     * @param  Request $request The HTTP Request.
     *
     * @return The HTTP Response.
     */
    public function getCountryAction(Request $request): Response
    {
        $ipAddress = $request->query->get("IP");
        $decimal = ip2long($ipAddress);
        $country = $this->countryRepository->getCountry($decimal);

        if ($country == null) {
            throw new Exception\NotFoundHttpException($this->getParameter("json_message_not_found"));
        }

        $response = new Response();
        $content  = json_encode($country);
        $response->setContent($content);
        $response->headers->set("Content-Type", "application/json");
        $response->setStatusCode(Response::HTTP_OK);

        return $response;
    }
}
