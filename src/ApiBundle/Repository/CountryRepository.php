<?php

namespace ApiBundle\Repository;

use ApiBundle\Entity\Country;
use Doctrine\ORM\EntityRepository;

/**
 * Country Repository
 */
class CountryRepository extends EntityRepository
{
    /**
     * Returns the Country that has in its decimal IP range a specific decimal IP value,
     * or returns null in case none exists
     *
     * @param  int          $decimal The decimal IP value to find the range for
     *
     * @return Country|null
     */
    public function getCountry(int $decimal): ?Country
    {
        $query = $this->getEntityManager()
            ->createQuery(
                "SELECT c "
                . " FROM ApiBundle:Country c"
                . " WHERE :decimal >= c.ipdecmin"
                . " AND :decimal <= c.ipdecmax"
        );
        $query->setParameter("decimal", $decimal);

        return $query->getOneOrNullResult();
    }
}
