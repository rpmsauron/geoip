geoip
=====

A Symfony project created on September 6, 2018, 7:27 pm.


Intro
=====

The project was developed under Ubuntu 16.04, with Symfony, and it is a take on the testing technical task located on the root of this project, under the filename 'Technical task PHP Developer - GeoIP database API.pdf':

Interpretation of the domain from the description of the requirements:

1. In terms of entity definition, to store the geoIP reference data, one single table seemed fit, since the data is fetched from a CSV file and its structure is pretty much table-wise.
1. The CSV file doesnt really explain what each particular column represents but after a little analysis, I came to the conclusion the column A and B are respectively the minimum and maximum IP addresses as a range for that particular country.
The columns C and D are the same, but in the decimal long representation of the IP address, and the E and F are respectively the Country affected to any IP address encompassed within the IP range.

Setup
=====

1. Install a webserver (Apache is advised which the development of the project was built upon), with PHP 7.1, and mysql.
Under:
`./vhost_example/`
there is an example of a virtualhost configuration to use for the project.

1. A few optional PHP extensions are required for the project to work, such as php-curl, php7.0-zip and php-zip.
From a Linux distribution environment, these can be installed by running the following commands:
`sudo apt-get install php-curl`
`sudo apt-get install php7.0-zip`
`sudo apt-get install php-zip`

1. Clone the Git repository from:
`git@bitbucket.org:rpmsauron/geoip.git`

1. Change directory into the root of the project, and, to fetch the vendor folder to get third party resources for Symfony project, run:
`composer install`

1. Use a REST client like Postman or Insomnia to test REST Requests of the API.
Insomnia is advised since settings and history of Requests for the project are provided inside:
`./GeoIP_2018-09-07.json`
which can be imported into Insomnia onto a workspace.

1. A database needs to be setup on mysql. The settings under:
`./app/config/parameters.yml`
assume a database named 'geoip', with access user 'root' and password 'karolina'.
If you use different values for these, the values for the database connection need to be changed as well in this file.


API Doc
=======

You can access the documentation of the API at:
`http://<localhost>/api/doc`
which will instruct how to use the API's endpoint(s).
Notice: the example in particular provided on the technical task does NOT match any country provided on the data fetched from the remote source data file, so, using that one in particular to test will assuredly return a 404 Not Found error. Use instead some IP address encapsulated in an actual existing IP range.


Populate Database
=================

To populate the database a command was developed to fetch the data from:
`http://geolite.maxmind.com/download/geoip/database/GeoIPCountryCSV.zip` 
The file is fetched into local storage at:
`./var/storage`
it is then uncompressed and its data is loaded onto the database.
The command must be run from the root of the project, and is:
`php bin/console db:populate`


Tests
=====

An integration test was developed for the API endpoint, which can be run on phpunit with:
`./vendor/bin/simple-phpunit tests/ApiBundle/Controller/ApiControllerTest.php --filter testGetCountryAction`


Contact
=====

Any doubts or questions feel free to contact me at:
`rpmsauron[NO_SPAM] AT gmail COM com`

Rui M. Silva, 2018